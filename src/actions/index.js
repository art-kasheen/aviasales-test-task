import {
  SET_STOPS_FILTER,
  SET_STOPS_FILTER_ONLY,
  SET_CURRENCY_FILTER
} from '../constants'

export function setStopsFilter(filter) {
  return {
    type: SET_STOPS_FILTER,
    payload: filter
  }
}

export function setStopsFilterOnly(filter) {
  return {
    type: SET_STOPS_FILTER_ONLY,
    payload: filter
  }
}

export function setCurrencyFilter(filter) {
  return {
    type: SET_CURRENCY_FILTER,
    payload: filter
  }
}
