import { createSelector } from 'reselect'

export const getTickets = (state) => state.tickets.entities
export const getFilterStops = (state) => state.filter.stops
export const getFilterCurrency = (state) => state.filter.currency
export const getFilterCurrencySelected = (state) =>
  state.filter.selectedCurrency
export const getFilterStopsUnchecked = (state) => state.filter.uncheckedStops

export const getTicketsList = createSelector(
  getTickets,
  (tickets) => Object.values(tickets)
)

export const getFilterStopsList = createSelector(
  getFilterStops,
  (filter) => Object.values(filter)
)

export const getFilterCurrencyList = createSelector(
  getFilterCurrency,
  (filter) => Object.values(filter)
)

export const getActiveCurrency = createSelector(
  getFilterCurrency,
  getFilterCurrencySelected,
  (filter, selected) => filter[selected]
)

export const getFilteredTickets = createSelector(
  getTicketsList,
  getFilterStopsUnchecked,
  (tickets, unchecked) =>
    tickets.filter((item) => !unchecked.includes(String(item.stops)))
)
