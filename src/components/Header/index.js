import React from 'react'
import logo from '../../assets/logo.svg'
import styles from './index.module.css'

export default () => {
  return (
    <div className={styles.wrapper}>
      <img src={logo} alt="logo" className={styles.logo} />
    </div>
  )
}
