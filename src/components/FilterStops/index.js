import React from 'react'
import PropsTypes from 'prop-types'
import styles from './index.module.css'
import CheckBox from '../common/CheckBox'
import { connect } from 'react-redux'
import { getFilterStopsList, getFilterStopsUnchecked } from '../../selectors'
import { setStopsFilter, setStopsFilterOnly } from '../../actions'

const FilterStops = ({
  filterItems,
  unchecked,
  setStopsFilter,
  setStopsFilterOnly
}) => {
  const handleChange = (ev) => {
    setStopsFilter(ev.target.name)
  }

  const handleChangeAll = () => {
    setStopsFilter(null)
  }

  const handleOnlyButtonClick = (id) => {
    setStopsFilterOnly(id)
  }

  const itemAll = (
    <li className={styles.item} key="all">
      <CheckBox
        onChange={handleChangeAll}
        title="Все"
        id="all"
        checked={!unchecked.length}
      />
    </li>
  )

  const items = filterItems.map((item) => (
    <li className={styles.item} key={item.id}>
      <CheckBox
        onChange={handleChange}
        title={item.title}
        id={item.id}
        hasOnlyButton={item.isShowOnly}
        checked={!unchecked.includes(item.id)}
        handleOlyButtonClick={handleOnlyButtonClick}
      />
    </li>
  ))

  return (
    <div>
      <h2 className={styles.title}>КОЛИЧЕСТВО ПЕРЕСАДОК</h2>

      <ul className={styles.list}>
        {itemAll}
        {items}
      </ul>
    </div>
  )
}

FilterStops.propTypes = {
  filterItems: PropsTypes.array.isRequired,
  setStopsFilter: PropsTypes.func,
  setStopsFilterOnly: PropsTypes.func,
  unchecked: PropsTypes.array
}

export default connect(
  (state) => ({
    filterItems: getFilterStopsList(state),
    unchecked: getFilterStopsUnchecked(state)
  }),
  { setStopsFilter, setStopsFilterOnly }
)(FilterStops)
