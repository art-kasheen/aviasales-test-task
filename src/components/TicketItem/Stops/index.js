import React from 'react'
import PropsTypes from 'prop-types'
import styles from './index.module.css'
import image from '../../../assets/stops.png'

const Stops = ({ stops }) => {
  const text = stops === 1 ? `${stops} пересадка` : `${stops} пересадок`

  return (
    <div className={styles.stops}>
      <span>{stops !== 0 && text}</span>
      <img src={image} alt="alt" />
    </div>
  )
}

Stops.propTypes = {
  stops: PropsTypes.number.isRequired
}

export default Stops
