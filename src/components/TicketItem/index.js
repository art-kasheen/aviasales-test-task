import React from 'react'
import PropTypes from 'prop-types'
import styles from './index.module.css'
import image from '../../assets/item-logo.png'
import BuyButton from './BuyButton'
import Stops from './Stops'
import Info from './Info'

const TicketItem = ({ ticket, currencyFilter }) => {
  const price = Math.round(ticket.price * currencyFilter.rate)
  const currency = currencyFilter.title

  return (
    <div className={styles.wrapper}>
      <div className={styles.left}>
        <img className={styles.image} src={image} alt="item logo" />
        <BuyButton price={price} currency={currency} />
      </div>

      <div className={styles.right}>
        <div className={styles.timeline}>
          <div className={styles.time}>{ticket.departure_time}</div>
          <Stops stops={ticket.stops} />
          <div className={styles.time}>{ticket.arrival_time}</div>
        </div>

        <div className={styles.cityline}>
          <Info
            destination={ticket.origin}
            destinationName={ticket.origin_name}
            date={ticket.departure_date}
          />
          <Info
            destination={ticket.destination}
            destinationName={ticket.destination_name}
            date={ticket.arrival_date}
            align="right"
          />
        </div>
      </div>
    </div>
  )
}

TicketItem.propTypes = {
  ticket: PropTypes.object.isRequired,
  currencyFilter: PropTypes.object
}

export default TicketItem
