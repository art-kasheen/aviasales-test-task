import React from 'react'
import PropTypes from 'prop-types'
import styles from './index.module.css'

const BuyButton = ({ price, currency }) => {
  return (
    <button className={styles.buyButton}>
      Купить за <br /> {price} {currency}
    </button>
  )
}

BuyButton.propTypes = {
  price: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired
}

export default BuyButton
