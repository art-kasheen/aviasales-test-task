import React from 'react'
import PropTypes from 'prop-types'
import styles from './index.module.css'

const Info = ({ destination, destinationName, date, align }) => {
  const day = new Date(date).toLocaleString('ru', { weekday: 'short' })
  const dateString = new Date(date).toLocaleString('ru', {
    month: 'short',
    day: 'numeric',
    year: 'numeric'
  })

  return (
    <div
      className={`${styles.wrapper} ${align === 'right' && styles.alignRight}`}
    >
      <p className={styles.city}>
        {align === 'left'
          ? `${destination}, ${destinationName}`
          : `${destinationName}, ${destination}`}
      </p>
      <p className={styles.date}>{`${dateString}, ${day}`}</p>
    </div>
  )
}

Info.propTypes = {
  destination: PropTypes.string.isRequired,
  destinationName: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  align: PropTypes.string
}

Info.defaultProps = {
  align: 'left'
}

export default Info
