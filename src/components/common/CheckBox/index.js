import React from 'react'
import PropTypes from 'prop-types'
import styles from './index.module.css'

const CheckBox = ({
  title,
  id,
  onChange,
  hasOnlyButton,
  handleOlyButtonClick,
  checked
}) => {
  return (
    <div className={styles.wrapper}>
      <input
        onChange={onChange}
        className={styles.input}
        id={id}
        name={id}
        checked={checked}
        type="checkbox"
      />
      <label className={styles.label} htmlFor={id}>
        {title}
      </label>
      {hasOnlyButton && (
        <button
          className={styles.button}
          onClick={() => handleOlyButtonClick(id)}
        >
          только
        </button>
      )}
    </div>
  )
}

CheckBox.propTypes = {
  title: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  hasOnlyButton: PropTypes.bool,
  handleOlyButtonClick: PropTypes.func,
  checked: PropTypes.bool.isRequired
}

export default CheckBox
