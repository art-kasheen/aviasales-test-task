import React from 'react'
import PropTypes from 'prop-types'
import styles from './index.module.css'

const Radio = ({ id, name, checked, onChange }) => {
  return (
    <div className={styles.field}>
      <input
        id={id}
        type="radio"
        name={name}
        className={styles.input}
        checked={checked}
        onChange={onChange}
      />
      <label className={styles.label} htmlFor={id}>
        {id}
      </label>
    </div>
  )
}

Radio.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired
}

export default Radio
