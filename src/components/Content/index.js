import React from 'react'
import styles from './index.module.css'
import Filters from '../Filters'
import TicketsList from '../TicketsList'

export default () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.container}>
        <div className={styles.left}>
          <Filters />
        </div>
        <div className={styles.right}>
          <TicketsList />
        </div>
      </div>
    </div>
  )
}
