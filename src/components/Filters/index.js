import React from 'react'
import styles from './index.module.css'
import FilterStops from '../FilterStops'
import FilterCurrency from '../FilterCurrency'

export default () => {
  return (
    <div className={styles.wrapper}>
      <FilterCurrency />
      <FilterStops />
    </div>
  )
}
