import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import TicketItem from '../TicketItem'
import { getFilteredTickets, getActiveCurrency } from '../../selectors'
import styles from './index.module.css'

const TicketsList = ({ filteredTickets, currencyFilter }) => {
  const items = filteredTickets.map((ticket) => (
    <li className={styles.item} key={ticket.id}>
      <TicketItem ticket={ticket} currencyFilter={currencyFilter} />
    </li>
  ))

  return <ul className={styles.list}>{items}</ul>
}

TicketsList.propTypes = {
  filteredTickets: PropTypes.array.isRequired,
  currencyFilter: PropTypes.object
}

export default connect((state) => ({
  filteredTickets: getFilteredTickets(state),
  currencyFilter: getActiveCurrency(state)
}))(TicketsList)
