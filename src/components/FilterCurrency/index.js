import React from 'react'
import PropTypes from 'prop-types'
import styles from './index.module.css'
import { connect } from 'react-redux'
import {
  getFilterCurrencyList,
  getFilterCurrencySelected
} from '../../selectors'
import { setCurrencyFilter } from '../../actions'
import Radio from '../common/Radio'

const FilterCurrency = ({ filters, setCurrencyFilter, selected }) => {
  const handleRadioChange = (ev) => {
    setCurrencyFilter(ev.target.id)
  }

  const items = filters.map((filter) => (
    <li key={filter.id} className={styles.item}>
      <Radio
        id={filter.id}
        name="currency"
        checked={filter.id === selected}
        onChange={handleRadioChange}
      />
    </li>
  ))

  return (
    <div className={styles.wrapper}>
      <h2 className={styles.title}>ВАЛЮТА</h2>

      <ul className={styles.list}>{items}</ul>
    </div>
  )
}

FilterCurrency.propTypes = {
  filters: PropTypes.array.isRequired,
  setCurrencyFilter: PropTypes.func
}

export default connect(
  (state, props) => ({
    filters: getFilterCurrencyList(state),
    selected: getFilterCurrencySelected(state)
  }),
  { setCurrencyFilter }
)(FilterCurrency)
