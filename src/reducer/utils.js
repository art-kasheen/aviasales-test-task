export const arrToMap = (arr) =>
  arr.reduce((acc, item) => {
    const id = Math.random()
      .toString(36)
      .substr(2, 9)
    return {
      [id]: {
        ...item,
        id
      },
      ...acc
    }
  }, {})
