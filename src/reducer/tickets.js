import { tickets } from '../tickets'
import { arrToMap } from './utils'

const initialState = {
  entities: arrToMap(tickets.list)
}

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
}
