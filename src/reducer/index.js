import { combineReducers } from 'redux'
import tickets from './tickets'
import filter from './filter'

const reducer = combineReducers({
  tickets,
  filter
})

export default reducer
