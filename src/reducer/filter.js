import {
  SET_STOPS_FILTER,
  SET_STOPS_FILTER_ONLY,
  SET_CURRENCY_FILTER
} from '../constants'

const initialState = {
  selectedCurrency: 'rub',
  uncheckedStops: [],
  currency: {
    rub: {
      id: 'rub',
      title: 'руб',
      rate: 1
    },
    usd: {
      id: 'usd',
      title: 'долл.',
      rate: 0.016
    },
    eur: {
      id: 'eur',
      title: 'евро',
      rate: 0.014
    }
  },
  stops: {
    0: {
      id: '0',
      title: 'Без пересадок',
      isShowOnly: true
    },
    1: {
      id: '1',
      title: '1 пересадка',
      isShowOnly: true
    },
    2: {
      id: '2',
      title: '2 пересадки',
      isShowOnly: true
    },
    3: {
      id: '3',
      title: '3 пересадки',
      isShowOnly: true
    }
  }
}

export default (state = initialState, action) => {
  const { type, payload } = action
  switch (type) {
    case SET_STOPS_FILTER:
      return {
        ...state,
        uncheckedStops:
          payload !== null
            ? state.uncheckedStops.includes(payload)
              ? state.uncheckedStops.filter((el) => el !== payload)
              : state.uncheckedStops.concat(payload)
            : state.uncheckedStops.length
            ? []
            : Object.keys(state.stops)
      }

    case SET_STOPS_FILTER_ONLY:
      return {
        ...state,
        uncheckedStops: Object.keys(state.stops).filter((o) => o !== payload)
      }

    case SET_CURRENCY_FILTER:
      return {
        ...state,
        selectedCurrency: payload
      }

    default:
      return state
  }
}
